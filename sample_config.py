# encoding: utf-8

hostname = "localhost"
host = "localhost"
port = 7000
document_root = "/var/gopher/"
plugins = "plugins"
strict = False
logging = "gopher.log"
format = "%(asctime)s [%(levelname)s] %(message)s"
format_date = "%Y-%m-%d %H:%M:%S"
sort = True
